import React from 'react'

const SpinLoader = (props) => {
    return (
            <div className="ui active dimmer">
                <div className="ui big text loader">{props.text}</div>
            </div>
    );
};

SpinLoader.defaultProps = {
  text: 'Loading...'
};

export default SpinLoader;