import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import SeasonDisplay from "./SeasonDisplay";
import SpinLoader from "./SpinLoader";
import useLocation from "./useLocation";

const App = () => {
    const [lat, errorMessage] = useLocation();
    let content;

    if (errorMessage) {
        content = <div>Error: {errorMessage}</div>;
    } else if (lat) {
        content = <SeasonDisplay lat={lat} />;
    } else {
        content = <SpinLoader
            text={"Please allow location !"}
        />;
    }

    return <div className={"border red"}>{content}</div>
};

ReactDOM.render(<App/>, document.querySelector("#root"));