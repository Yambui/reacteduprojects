import React from 'react';
import {connect} from 'react-redux';

const SongDetail = (props) => {
    if (props.selectedSong) {
        return (
            <div>
                <h3> Details for :</h3>
                <p>{props.selectedSong.title}</p>
                <h3> Length :</h3>
                <p>{props.selectedSong.duration}</p>
            </div>
        );
    } else {
        return <div>Select Song!</div>;
    }
};

const mapStateToProps = (state) => {
    return {selectedSong: state.selectedSong};
};

export default connect(mapStateToProps)(SongDetail);