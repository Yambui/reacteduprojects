import {combineReducers} from "redux";

const songReducer = () => {
  return [
      {title: 'Barbie', duration: '2:30'},
      {title: 'Makaka', duration: '12:30'},
      {title: 'Burbak', duration: '3:30'},
      {title: 'Baburak', duration: '5:30'},
  ]
};

const selectedSongReducer = (selectedSong=null, action) => {
    if (action.type === 'SONG_SELECTED') {
        return action.payload;
    }
    return selectedSong;
};

export default combineReducers({
    songs: songReducer,
    selectedSong: selectedSongReducer
});