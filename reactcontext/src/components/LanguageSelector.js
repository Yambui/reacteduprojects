import React from 'react';
import LanguageContext from '../contexts/LanguageContext';

class LanguageSelector extends React.Component {
static contextType = LanguageContext;

render () {
        return (
             <div>
                 <h1>
                     Select language:
                      <i className={'flag us large'} onClick={() => this.context.onLanguageChange('english')}/>
                      <i className={'flag ua large'} onClick={() => this.context.onLanguageChange('ukrainian')}/>
                 </h1>
             </div>
        );
    }
}

export default LanguageSelector;