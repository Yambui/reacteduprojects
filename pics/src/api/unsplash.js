import axios from 'axios';

export default  axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID 02c61aceb149638b321b46a6133b59a9ca4557c203185e43a433aff62f527766'
    }
});