import React from 'react';
import { connect } from 'react-redux';

import { fetchPostAndUsers } from "../actions";
import UserName from "./UserName";

class PostList extends React.Component {
    componentDidMount() {
        this.props.fetchPostAndUsers();
    }

    renderList() {
        return this.props.posts.map(post => {
            return (
                <div className={"item"} key={post.id}>
                    <i className={"large middle alligned icon user"}/>
                    <div className={"content"}>
                        <div className={"description"}>
                            <h2>{post.title}</h2>
                            <p>{post.body}</p>
                            <h3><UserName id={post.userId}/></h3>
                        </div>
                    </div>
                </div>
            );
        });
    }

    render() {
        console.log(this.props.posts);
        return <div className={"ui relaxed divided list"}>{this.renderList()}</div>
    }
}

const mapStateToProps = (state) => {
    return {posts: state.posts,
            };
}

export default connect(mapStateToProps, { fetchPostAndUsers })(PostList);