console.clear();

const deletePolicy = (name) => {
    return {
        type: 'DELETE_POLICY',
        payload: {
            name: name
        }
    };
};

const createClaim = (name, amount) => {
    return {
        type: 'CREATE_CLAIM',
        payload: {
            name: name,
            money: amount
        }
    };
};

const createPolicy = (name, amount) => {
    return {
        type: 'CREATE_POLICY',
        payload: {
            name: name,
            amount: amount
        }
    };
};

//Reducers
const accounting = (bagOfMoney = 100, action) => {
    if (action.type === 'CREATE_CLAIM') {
        return bagOfMoney - action.payload.money;
    } else if (action.type === 'CREATE_POLICY') {
        return bagOfMoney + action.payload.amount;
    }
    return bagOfMoney;
};

const policyDep = (listOfPolicies = [], action) => {
    if (action.type === 'CREATE_POLICY') {
        return [...listOfPolicies, action.payload.name];
    } else if (action.type === 'DELETE_POLICY') {
        return listOfPolicies.filter(name => name !== action.payload.name);
    }
    return listOfPolicies;
};

const claimsHistory = (listOfClaims = [], action) => {
    if (action.type === 'CREATE_CLAIM') {
        return [...listOfClaims, action.payload];
    }
    return listOfClaims;
};


const { createStore, combineReducers} = Redux;

const outDepartments = combineReducers({
    accounting: accounting,
    claimsHistory: claimsHistory,
    policyDep: policyDep
});

const store = createStore(outDepartments);

store.dispatch(createPolicy('Alex', 200));
store.dispatch(createPolicy('Jane', 200));
store.dispatch(createPolicy('jax', 2000));
store.dispatch(createPolicy('Bob', 120));

store.dispatch(createClaim('Bob', 1950));

store.dispatch(deletePolicy('Alex'));

console.log(store.getState());