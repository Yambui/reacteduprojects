import axios from 'axios';

const KEY ='AIzaSyBdNbv-umKo97rjcEyH4cDLsHLzN8C8uuQ';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResults: 5,
        key: KEY
    }
});

